/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora2;

import com.sun.glass.events.KeyEvent;
import javax.swing.*;

public class Calculadora_Grafica extends javax.swing.JFrame  {

    public Calculadora_Grafica() {
        initComponents();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jMenu1 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        numero1 = new javax.swing.JTextField();
        numero2 = new javax.swing.JTextField();
        btblimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        resultado = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnsuma = new javax.swing.JButton();
        btnresta = new javax.swing.JButton();
        btnmultiplicacion = new javax.swing.JButton();
        btndivicion = new javax.swing.JButton();
        btncalculo_de_edad = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jMenu1.setText("jMenu1");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        numero1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numero1ActionPerformed(evt);
            }
        });

        btblimpiar.setText("limpiar");
        btblimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btblimpiarActionPerformed(evt);
            }
        });

        jLabel1.setText("ingrese primer numero");

        jLabel2.setText("ingrese el seg numero");

        resultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultadoActionPerformed(evt);
            }
        });

        jLabel3.setText("Resultado");

        btnsuma.setText("Suma");
        btnsuma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsumaActionPerformed(evt);
            }
        });
        btnsuma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                btnsumaKeyTyped(evt);
            }
        });

        btnresta.setText("Resta");
        btnresta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrestaActionPerformed(evt);
            }
        });

        btnmultiplicacion.setText("Mult");
        btnmultiplicacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmultiplicacionActionPerformed(evt);
            }
        });

        btndivicion.setText("Divicion");
        btndivicion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndivicionActionPerformed(evt);
            }
        });

        btncalculo_de_edad.setText("Calculo de edad");
        btncalculo_de_edad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncalculo_de_edadActionPerformed(evt);
            }
        });

        jButton1.setText("Calculadora avanzada");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cerra ");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(numero1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(81, 81, 81))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(numero2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(jLabel3)
                        .addGap(37, 37, 37)
                        .addComponent(btblimpiar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(btnsuma)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnresta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(btnmultiplicacion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btndivicion))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btncalculo_de_edad, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(resultado, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(numero1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numero2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(btblimpiar))
                .addGap(1, 1, 1)
                .addComponent(resultado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnresta)
                    .addComponent(btnsuma))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btndivicion)
                    .addComponent(btnmultiplicacion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(btncalculo_de_edad)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsumaActionPerformed
         jLabel4.setText("");
    
         if(numero1.getText().isEmpty()||numero2.getText().isEmpty() ){
              // JOptionPane.showMessageDialog(null, "Espacion en Blanco, inserte un numero "
               //        +"", "",
               //JOptionPane.ERROR_MESSAGE );
             //  return ;
              // isEmpty significa "espacio em blanco"
              
            }
         //double num1 = Double.parseDouble(numero1.getText());
         // double num2 = Double.parseDouble(numero2.getText());
        //  resultado.setText(String.valueOf(num1+num2));
       

  resultado.setText(String.valueOf(Calculador1.operacion(12, 12, '+')));
 
  
    //este codido servira para insertar los valores por los parametros y y esta conectado con la clase calculadora1
               
      
      
    }//GEN-LAST:event_btnsumaActionPerformed

    private void btblimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btblimpiarActionPerformed
        if (numero1.getText().isEmpty()||numero2.getText().isEmpty()) {
            jLabel4.setText("Espacio en blanco");
            //JOptionPane.showMessageDialog(null, "No ahi dana que limpiar"+ "", "",JOptionPane.INFORMATION_MESSAGE);
        }
        
        
  
 resultado.setText(null);
numero1.setText(null);
numero2.setText(null);
//System.inh



    }//GEN-LAST:event_btblimpiarActionPerformed

    private void btnrestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrestaActionPerformed
if(numero1.getText().isEmpty()||numero2.getText().isEmpty()){
    
    JOptionPane.showMessageDialog(null, "Introduca un numero"
            + "", "",
            JOptionPane.ERROR_MESSAGE);
    return;    
      }
      double num1 = Double.parseDouble(numero1.getText());
      double num2 = Double.parseDouble(numero2.getText());
      resultado.setText(String.valueOf(num1-num2)); 
    }//GEN-LAST:event_btnrestaActionPerformed

    private void btnmultiplicacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmultiplicacionActionPerformed
        if (numero1.getText().isEmpty()||numero2.getText().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "introdusca un numero"
                    + "", "",JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        double num1 = Double.parseDouble(numero1.getText());
       double num2 = Double.parseDouble(numero2.getText());
       resultado.setText(String.valueOf(num1*num2));
    }//GEN-LAST:event_btnmultiplicacionActionPerformed

    
    private void resultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultadoActionPerformed
       
    }//GEN-LAST:event_resultadoActionPerformed

    private void numero1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numero1ActionPerformed
        
     
    }//GEN-LAST:event_numero1ActionPerformed

    private void btndivicionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndivicionActionPerformed
        if (numero1.getText().isEmpty()||numero2.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Introduca un numero"+ "", "",JOptionPane.ERROR_MESSAGE);
        return;
        }
        double num1= Double.parseDouble(numero1.getText());
        double num2 = Double.parseDouble(numero2.getText());
        resultado.setText(String.valueOf(num1/num2));
        
    }//GEN-LAST:event_btndivicionActionPerformed

    private void btncalculo_de_edadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncalculo_de_edadActionPerformed
        
        int año_actual,año_nacimiento,edad;
        
        if (JOptionPane.showInputDialog("Ingresa tu año actual:").isEmpty()
                
          
            
               
            ||JOptionPane.showInputDialog("Ingresa tu año de nacimineto").isEmpty())
            JOptionPane.showMessageDialog(null, "No puede estar en blanco"+"", "",JOptionPane.ERROR_MESSAGE);
       
           
             año_actual= Integer.parseInt(JOptionPane.showInputDialog("Ingresa tu año actual: "));
             año_nacimiento=Integer.parseInt(JOptionPane.showInputDialog("ingresa año de nacimiento: "));
             edad= año_actual - año_nacimiento;
              JOptionPane.showMessageDialog(null, "su edad es: "+edad); 
              
              
                    
    }//GEN-LAST:event_btncalculo_de_edadActionPerformed

    private void btnsumaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnsumaKeyTyped
 
        char suma=evt.getKeyChar();
        if (suma==KeyEvent.VK_ENTER) {
           // btnsuma.doClick();
           btnsuma.doClick(suma);
        }
        
    }//GEN-LAST:event_btnsumaKeyTyped

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        Calculadora_avanzada avan = new Calculadora_avanzada();
       avan.setVisible(true);
       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       System.exit(0);
       
       
      
       
    }//GEN-LAST:event_jButton2ActionPerformed

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculadora_Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculadora_Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculadora_Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculadora_Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               // new Calculadora_Grafica().setVisible(true);
            }
        });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btblimpiar;
    private javax.swing.JButton btncalculo_de_edad;
    private javax.swing.JButton btndivicion;
    private javax.swing.JButton btnmultiplicacion;
    private javax.swing.JButton btnresta;
    private javax.swing.JButton btnsuma;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JTextField numero1;
    private javax.swing.JTextField numero2;
    private javax.swing.JTextField resultado;
    // End of variables declaration//GEN-END:variables
}
